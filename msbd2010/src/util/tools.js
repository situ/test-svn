/**
 * 根据分钟返回格式化后的时间字符串
 * @param {*} minutes 
 * @returns "00:00"
 */
export const formatTime = secondes => {
  if (!secondes) {
    return "00:00"
  }
  // const secondes = minutes * 60
  const m = Math.floor(secondes / 60)
  const s = secondes % 60
  return fill0(m) + ':' + fill0(s)
}

/**
 * 小于10的数字补0
 * @param {*} num 
 */
export const fill0 = num => {
  return num < 10 ? '0' + num : num
}