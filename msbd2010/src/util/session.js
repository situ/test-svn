const USER_LOGIN_INFO = 'bk-2010-user-key'
const ss = sessionStorage

export const saveUser = (user) => {
  ss.setItem(USER_LOGIN_INFO, JSON.stringify(user))
}

export const getUser = () => {
  return ss.getItem(USER_LOGIN_INFO)
}

export const removeUser = () => {
  ss.removeItem(USER_LOGIN_INFO)
}