import request from '../util/request'
import qs from 'qs'

export const getListApi = (params) =>
  request({
    url: 'msbd/list',
    method: 'get',
    params
  })

export const getStudyBdidByUidApi = id =>
  request({
    url: 'msbd/studybdid-list',
    method: 'get',
    params: { uid: id }
  })
