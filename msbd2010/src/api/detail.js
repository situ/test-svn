import request from '../util/request'
import qs from 'qs'

export const saveCommentApi = data =>
  request({
    url: 'detail/save-comment',
    method: 'post',
    data: qs.stringify(data)
  })

export const getCommentListApi = bdid =>
  request({
    url: 'detail/get-comment-list',
    method: 'get',
    params: { bdid }
  })

export const getItemApi = id =>
  request({
    url: 'detail/get-item',
    method: 'get',
    params: { id }
  })

export const saveStudyApi = data =>
  request({
    url: 'detail/save-study',
    method: 'post',
    data: qs.stringify(data)
  })