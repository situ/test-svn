import request from '../util/request'
import qs from 'qs'

export const doLoginApi = data =>
  request({
    url: 'user/login',
    method: 'post',
    data: qs.stringify({ loginName: data.username, loginKey: data.password })
  })
