import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Home,
    children: [
      {
        path: '',
        component: () => import('../views/home/index.vue')
      },
      {
        path: 'msbd',
        component: () => import('../views/msbd/index.vue')
      },
      {
        path: 'ks',
        component: () => import('../views/ks/index.vue')
      },
      {
        path: 'sz',
        component: () => import('../views/sz/index.vue')
      },
      {
        path: '/detail/:id',
        name: 'detail',
        component: () => import('../views/msbd/detail.vue'),
        props: true
      }
    ]
  },
  {
    path: '/login',
    component: () => import('../views/login.vue'),
  },
  {
    path: '/about',
    component: () => import('../views/About.vue'),
  }
  // {
  //   path: '/detail/:id',
  //   name: 'detail',
  //   component: () => import('../views/msbd/detail.vue'),
  //   props: true
  // }
]

const router = new VueRouter({
  routes
})

const whiteList = ['/sz', '/login']

router.beforeEach((to, from, next) => {
  const user = store.state.login.user
  if (whiteList.includes(to.path)) {
    next()
  } else {
    if (user.id) {
      next()
    } else {
      next('/sz')
    }
  }
})

export default router
