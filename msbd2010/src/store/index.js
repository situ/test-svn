import Vue from 'vue'
import Vuex from 'vuex'
import msbd from './modules/msbd'
import detail from './modules/detail'
import login from './modules/login'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    msbd,
    detail,
    login
  }
})
