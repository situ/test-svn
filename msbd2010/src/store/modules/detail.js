import { saveCommentApi, getCommentListApi, getItemApi, saveStudyApi } from '../../api/detail'

const state = {
  list: [],
  item: {}
}
const mutations = {
  SET_LIST(state, list) {
    state.list = list
  },
  SET_ITEM(state, item) {
    state.item = item
  }
}
const actions = {
  async saveComment({ rootState }, commentObj) {
    const { id } = rootState.login.user;
    commentObj.uid = id
    await saveCommentApi(commentObj)
  },
  async getList({ commit }, bdid) {
    const { data } = await getCommentListApi(bdid)
    commit('SET_LIST', data)
  },
  async getItemById({ commit }, id) {
    const { data } = await getItemApi(id)
    commit('SET_ITEM', data)
  },
  async saveStudy({ rootState }, bdid) {
    const { id } = rootState.login.user;
    await saveStudyApi({ bdid, uid: id })
  }
}
const getters = {
  commentCount(state) {
    return state.list.length
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
