import { getListApi, getStudyBdidByUidApi } from '../../api/msbd'

export default {
  namespaced: true,
  state: {
    list: [],
    currentPage: 0,
    detailFooterStatus: true, // true：正常状态; false：评论状态
    studyIds: []
  },
  mutations: {
    SET_STUDY_IDS(state, ids) {
      state.studyIds = ids
    },
    SET_LIST(state, list) {
      state.list = state.list.concat(list)
    },
    RESET_LIST(state) {
      state.list = []
    },
    SET_CURRENT_PAGE(state, page) {
      state.currentPage = page
    },
    SET_DETAILFOOTERSTATUS(state, status) {
      state.detailFooterStatus = status
    }
  },
  actions: {
    async getStudyBdidByUid({ commit, rootState }) {
      const { id } = rootState.login.user;
      const { data } = await getStudyBdidByUidApi(id)
      commit('SET_STUDY_IDS', data)
    },
    async getList({ commit, state }) {
      const { data } = await getListApi({ currentPage: state.currentPage })
      commit('SET_LIST', data)
    }
  },
  getters: {}
}