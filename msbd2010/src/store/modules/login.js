import { doLoginApi } from '../../api/login'
import { saveUser, getUser } from '../../util/session'

const state = {
  user: JSON.parse(getUser() || '{}')
}
const mutations = {
  SET_USER(state, user) {
    state.user = user
  }
}
const actions = {
  async doLogin({ commit }, form) {
    const { data } = await doLoginApi(form)
    saveUser(data)
    commit('SET_USER', data)
  }
}
const getters = {
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
