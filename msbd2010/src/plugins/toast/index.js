import bkToast from './bkToast'

const toast = {
  install(Vue) {
    const ToastConstructor = Vue.extend(bkToast)
    const instance = new ToastConstructor()
    instance.$mount(document.createElement('div'))
    document.body.appendChild(instance.$el)
    // console.log(instance);
    Vue.prototype.$toast = (msg, duration = 2000) => {
      if (instance.visible) return;
      instance.message = msg;
      instance.visible = true;
      setTimeout(() => {
        instance.visible = false;
      }, duration);
    }

  }
}

export default toast